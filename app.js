const express = require("express");
const axios = require("axios");
const app = express();
var cors = require('cors');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
const dotenv = require('dotenv');
dotenv.config();
// ! extract these to .env file
const clientId =process.env.CLIENT_ID;
const clientSecret =process.env.CLIENT_SECRET;
const appId = process.env.APP_ID;
const token =process.env.TOKEN;

const region= parseInt(process.env.REGION);
const bucket=process.env.BUCKET;
const accessKey= process.env.ACCESS_KEY;
const secretKey= process.env.SECRET_KEY;

console.log(`${Buffer.from(`${clientId}:${clientSecret}`).toString("base64")}`);

app.get("/", (req, res) => res.send("Agora Cloud Recording Server"));

app.post("/acquire", async (req, res) => {
  let data =  {
    "cname":req.body.channel,
    "uid":req.body.uid.toString(),
    "clientRequest":{
      "resourceExpiredHour":24,
      "scene":0}};
  let config = {
    method: 'post',
    url: `https://api.agora.io/v1/apps/${appId}/cloud_recording/acquire`,
    headers: { 
      'Authorization': `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString("base64")}`, 
      'Content-Type': 'application/json'
    },
    data : JSON.stringify(data)
  };
  console.log("the request parms----------------------------------",config);
  axios(config)
.then(function (response) {
  res.send(response.data);
})
.catch(function (error) {
  res.send(error);
});

});

app.post("/start", async (req, res) => {
  console.log("-----------Start--------------------",req.body)
  var axios = require('axios');
  var data = JSON.stringify({
    "token":token,
    cname:req.body.channel,
    uid:req.body.uid.toString(),
    clientRequest: {
      token:token,
      "recordingConfig":{
        "channelType":0,
        "streamTypes":2,
        "audioProfile":1,
        "videoStreamType":0,
        "maxIdleTime":120,
        "transcodingConfig":{
          "width":360,
          "height":640,
          "fps":30,
          "bitrate":600,
          "maxResolutionUid":"1",
          "mixedVideoLayout":1,
          backgroundColor: "#FFFFFF",
          }
        
      },
      recordingFileConfig: {
        avFileType: ["hls"],
      },
      storageConfig: {
        vendor: 1,
        region:region,
        bucket:bucket,
        accessKey:accessKey,
        secretKey:secretKey,
        fileNamePrefix: ["directory1", "directory2"],
      },
    },
      });
  var config = {
    method: 'post',
    url: `https://api.agora.io/v1/apps/${appId}/cloud_recording/resourceid/${req.body.resource.resourceId}/mode/${req.body.mode}/start`,
    headers: { 
      'Authorization': `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString("base64")}`, 
      'Content-Type': 'application/json'
    },
    data : data
  };
  
  axios(config)
  .then(function (response) {
    console.log("the response start Done");
    res.send(response.data);
  })
  .catch(function (error) {
    console.log("the response Errrrrrrrroooooooooooooor  start Done",error);
    res.send(error);
  });
});

app.post("/stop", async (req, res) => {
  const appID = appId;
  console.log("on stop methode resource 1",req.body)
  const resource = req.body.resource.resourceId;
  const sid = req.body.sid;

  const mode = req.body.mode;
  var axios = require('axios');
  var data = JSON.stringify({
    "token":token,
    "cname":"test4",
    "uid":req.body.uid.toString(),
    "mode": mode,
    "sid": sid,
    "resource":resource,
    "clientRequest": {
      token:token
    }
  })
  var config = {
    method: 'post',
    url: `https://api.agora.io/v1/apps/${appID}/cloud_recording/resourceid/${resource}/sid/${sid}/mode/${mode}/stop`,
    headers: { 
      'Authorization': `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString("base64")}`, 
      'Content-Type': 'application/json'
    },
    data : data
  };

  axios(config)
  .then(function (response) {
    console.log("stop video data",response.data)
    res.send(response.data);
  })
  .catch(function (error) {
    console.log("error")
    res.send(error);
  });
});

const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`Agora Cloud Recording Server listening at Port ${port}`));
